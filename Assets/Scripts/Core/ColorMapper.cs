﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Core
{
    public class ColorMapper : MonoBehaviour
    {
        [SerializeField] private Color[] colors;

        public int GetRandomColorIndex()
        {
            return Random.Range(0, colors.Length);
        }

        public Color ToColor(int i )
        {
            Assert.IsTrue(i >= 0 && i < colors.Length);
            return colors[i];
        }

        private void Awake()
        {
            Assert.IsTrue(colors.Length != 0);
        }
    }
}