﻿using System.Collections.Generic;

namespace Core
{
    public readonly struct BoardLine
    {
        private BoardPoint A { get; }
        private BoardPoint B { get; }

        public BoardLine(BoardPoint a, BoardPoint b)
        {
            A = a;
            B = b;
        }

        private sealed class CommutableEqualityComparer : IEqualityComparer<BoardLine>
        {
            public bool Equals(BoardLine lhs, BoardLine rhs)
            {
                return lhs.A.Equals(rhs.A) && lhs.B.Equals(rhs.B) ||
                       lhs.A.Equals(rhs.B) && lhs.B.Equals(rhs.A) ;
            }

            public int GetHashCode(BoardLine obj)
            {
                return obj.A.GetHashCode() ^ obj.B.GetHashCode();
            }
        }

        public static IEqualityComparer<BoardLine> CommutableComparer { get; } = new CommutableEqualityComparer();
    }
}