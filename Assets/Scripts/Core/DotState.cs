﻿namespace Core
{
    public enum DotState
    {
        Still,
        Moving,
        Destroyed
    }
}