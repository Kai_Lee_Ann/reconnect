﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace Core
{
    [RequireComponent(typeof(LineFactory))]
    public class DotLineChain : MonoBehaviour
    {
        private const int MinChainSize = 2;

        [SerializeField] private Board board;
        [SerializeField] private ColorMapper colorMapper;
        [SerializeField] private float lineWidth;

        public Action<List<Dot>> OnChainCompleted;

        private List<Dot> _dots;
        private List<BoardLine> _lines;
        private Dot _lastDot;
        private Dot _secondLast;
        private LineFactory _lineFactory;
        private Line _mouseLine;
        private Camera _camera;
        private Stack<Line> _drawLinesStack;

        private void Awake()
        {
            _dots = new List<Dot>();
            _lines = new List<BoardLine>();
            _lineFactory = GetComponent<LineFactory>();
            _drawLinesStack = new Stack<Line>();
            Debug.Assert(_camera != null, "Camera.main != null");
            _camera = Camera.main;
        }

        public void OnDotSelected(Dot dot)
        {
            if (_dots.Count == 0)
            {
                AddFirstDotToChain(dot);
                dot.PlaySelectAnimation();
                return;
            }

            if (dot == _secondLast)
            {
                RemoveLastDotFromChain();
                return;
            }

            if (!_lastDot.BoardPoint.IsNeighbor(dot.BoardPoint))
            {
                return;
            }

            if (_lastDot.ColorIndex != dot.ColorIndex)
            {
                return;
            }

            var boardLine = new BoardLine(_lastDot.BoardPoint, dot.BoardPoint);
            if (_lines.Contains(boardLine, BoardLine.CommutableComparer))
            {
                return;
            }

            if (_dots.Contains(dot))
            {
                PlaySelectAnimation(_dots);
                PlaySelectAnimation(board.GetDots(dot.ColorIndex));
            }

            AddDotToChain(dot, boardLine);
            dot.PlaySelectAnimation();
        }
        
        private static void PlaySelectAnimation(IEnumerable<Dot> dots)
        {
            foreach (var dot in dots)
            {
                dot.PlaySelectAnimation();
            }
        }

        private void AddFirstDotToChain(Dot dot)
        {
            _dots.Add(dot);
            _lastDot = dot;
        }

        private void RemoveLastDotFromChain()
        {
            _dots.RemoveAt(_dots.Count - 1);
            _lines.RemoveAt(_lines.Count - 1);
            _lastDot = _dots.Last();
            _secondLast = _dots.Count >= 2
                ? _dots[_dots.Count - 2]
                : null;
            _mouseLine.gameObject.SetActive(false);
            _mouseLine = null;
            _drawLinesStack.Pop().gameObject.SetActive(false);
        }

        private void AddDotToChain(Dot dot, BoardLine boardLine)
        {
            _secondLast = _lastDot;
            _lastDot = dot;
            _lines.Add(boardLine);
            _dots.Add(dot);
            _drawLinesStack.Push(
                _lineFactory.GetLine(
                    board.ToWorldPoint(_secondLast.BoardPoint),
                    board.ToWorldPoint(_lastDot.BoardPoint),
                    lineWidth,
                    colorMapper.ToColor(_lastDot.ColorIndex)
                )
            );
            _mouseLine.gameObject.SetActive(false);
            _mouseLine = null;
        }

        private void Update()
        {
            if (_dots.Count == 0)
            {
                return;
            }

            if (Input.GetMouseButtonUp(0))
            {
                ClearDots();
                ClearChain();
                return;
            }

            if (Input.GetMouseButton(0))
            {
                DrawMouseLine();
            }
        }

        private void DrawMouseLine()
        {
            if (ReferenceEquals(_mouseLine, null))
            {
                _mouseLine = _lineFactory.GetLine(
                    board.ToWorldPoint(_lastDot.BoardPoint),
                    _camera.ScreenToWorldPoint(Input.mousePosition),
                    lineWidth,
                    colorMapper.ToColor(_lastDot.ColorIndex)
                );
            }

            _mouseLine.end = _camera.ScreenToWorldPoint(Input.mousePosition);
        }

        private void ClearDots()
        {
            if (_dots.Count < MinChainSize)
            {
                return;
            }
            if (IsChainContainsClosedShape())
            {
                _dots.AddRange(board.GetDots(_lastDot.ColorIndex));
            }
            OnChainCompleted.Invoke(_dots);
        }

        private void ClearChain()
        {
            _dots.Clear();
            _lines.Clear();
            _lastDot = null;
            _secondLast = null;
            _mouseLine = null;
            foreach (var line in _lineFactory.GetActive())
            {
                line.gameObject.SetActive(false);
            }
        }

        private bool IsChainContainsClosedShape()
        {
            return _dots.Distinct().Count() != _dots.Count;
        }

        private void OnDrawGizmos()
        {
            DrawGizmoSquaresOnPath(new Color(255f, 0f, 0f, 0.25f));
            DrawGizmoSquareOnLastDot(new Color(255f, 255f, 0f, 0.25f));
            DrawGizmoSquareOnSecondLastDot(new Color(255f, 0f, 255f, 0.25f));
        }

        private void DrawGizmoSquareOnLastDot(Color color)
        {
            if (_lastDot == null)
            {
                return;
            }

            Gizmos.color = color;
            Gizmos.DrawCube(board.ToWorldPoint(_lastDot.BoardPoint), Vector3.one);
        }

        private void DrawGizmoSquareOnSecondLastDot(Color color)
        {
            if (_secondLast == null)
            {
                return;
            }

            Gizmos.color = color;
            Gizmos.DrawCube(board.ToWorldPoint(_secondLast.BoardPoint), Vector3.one);
        }

        private void DrawGizmoSquaresOnPath(Color color)
        {
            if (_dots == null)
            {
                return;
            }

            foreach (var dot in _dots)
            {
                Gizmos.color = color;
                Gizmos.DrawCube(board.ToWorldPoint(dot.BoardPoint), Vector3.one);
            }
        }
    }
}