﻿using System;

namespace Core
{
    public readonly struct BoardPoint
    {
        public readonly int Column;
        public readonly int Row;

        public BoardPoint(int column, int row)
        {
            Column = column;
            Row = row;
        }

        public bool IsNeighbor(BoardPoint boardPoint)
        {
            return Math.Abs(Row - boardPoint.Row) + Math.Abs(Column - boardPoint.Column) == 1; 
        }

        public bool Equals(BoardPoint other)
        {
            return Column == other.Column && Row == other.Row;
        }

        public override bool Equals(object obj)
        {
            return obj is BoardPoint other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Column * 397) ^ Row;
            }
        }

        public override string ToString()
        {
            return $"DotPoint(Column={Column}, Row={Row})";
        }
    }
}