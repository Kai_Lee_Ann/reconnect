using System;
using System.Collections;
using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class Dot : MonoBehaviour
    {
        public AnimationCurve fallDownCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
        public AnimationCurve aboveFallDownCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

        [SerializeField] private float destroyTime;
        [SerializeField] private float fallDownTime;
        [SerializeField] private float aboveFallDownTime;
        [SerializeField] private float shuffleTime;
        [SerializeField] private float selectTime;

        public int ColorIndex { get; set; }
        public BoardPoint BoardPoint { get; set; }
        public SpriteRenderer SelectEffectRenderer { get; private set; }
        public Action<Dot> OnDotSelected { get; set; }

        private DotState _state;
        private IEnumerator _selectAnimation;
        private IEnumerator _fallToPosition;
        private IEnumerator _aboveFallToPosition;

        private void Awake()
        {
            _state = DotState.Still;
            SelectEffectRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        }

        private void OnMouseDown()
        {
            SelectDot();
        }

        private void OnMouseEnter()
        {
            if (!Input.GetMouseButton(0))
            {
                return;
            }

            SelectDot();
        }

        private void SelectDot()
        {
            if (_state != DotState.Still)
            {
                return;
            }

            OnDotSelected.Invoke(this);
        }

        public void FallDown(Vector3 position, float delay)
        {
            if (_aboveFallToPosition != null)
            {
                FallDownFromAbove(position, delay);
                return;
            }
            
            if (_fallToPosition != null)
            {
                StopCoroutine(_fallToPosition);
            }

            _state = DotState.Moving;
            _fallToPosition = FallDownToPosition(position, delay);
            StartCoroutine(_fallToPosition);
        }

        public void FallDownFromAbove(Vector3 position, float delay)
        {
            if (_aboveFallToPosition != null)
            {
                StopCoroutine(_aboveFallToPosition);
            }

            _state = DotState.Moving;
            _aboveFallToPosition = FallDownFromAboveToPosition(position, delay);
            StartCoroutine(_aboveFallToPosition);
        }

        public void Move(Vector3 position)
        {
            _state = DotState.Moving;
            StartCoroutine(MoveToPosition(position));
        }

        private IEnumerator MoveToPosition(Vector3 targetPosition)
        {
            var startPosition = transform.position;
            var time = 0f;
            while (time < 1)
            {
                time += 5f * Time.deltaTime / shuffleTime;
                transform.position = Vector3.Lerp(startPosition, targetPosition, time);
                yield return null;
            }

            _state = DotState.Still;
        }

        private IEnumerator FallDownToPosition(Vector3 targetPosition, float delay)
        {
            yield return new WaitForSeconds(delay);
            var startPosition = transform.position;
            var time = 0f;
            while (time < 1)
            {
                time += 5f * Time.deltaTime / fallDownTime;
                transform.position = startPosition + (targetPosition - startPosition) * fallDownCurve.Evaluate(time);
                yield return null;
            }

            _fallToPosition = null;
            _state = DotState.Still;
        }

        private IEnumerator FallDownFromAboveToPosition(Vector3 targetPosition, float delay)
        {
            yield return new WaitForSeconds(delay);
            var startPosition = transform.position;
            var time = 0f;
            while (time < 1)
            {
                time += 5f * Time.deltaTime / aboveFallDownTime;
                transform.position = startPosition + (targetPosition - startPosition) * aboveFallDownCurve.Evaluate(time);
                yield return null;
            }

            _aboveFallToPosition = null;
            _state = DotState.Still;
        }


        public void Destroy(Action callback)
        {
            if (_state == DotState.Destroyed)
            {
                return;
            }

            _state = DotState.Destroyed;
            StartCoroutine(PlayDestroyAnimation(callback));
        }

        private IEnumerator PlayDestroyAnimation(Action callback)
        {
            SelectEffectRenderer.enabled = false;
            var scale = transform.localScale;
            var time = 0f;
            while (time < 1)
            {
                time += 5f * Time.deltaTime / destroyTime;
                transform.localScale = Vector3.Lerp(scale, Vector3.zero, time);
                yield return null;
            }

            callback.Invoke();
        }

        public void PlaySelectAnimation()
        {
            if (_state != DotState.Still)
            {
                return;
            }

            ResetSelectAnimation(SelectEffectRenderer);

            if (_selectAnimation != null)
            {
                StopCoroutine(_selectAnimation);
            }

            _selectAnimation = PlaySelectAnimation(SelectEffectRenderer);
            StartCoroutine(_selectAnimation);
        }

        private IEnumerator PlaySelectAnimation(SpriteRenderer spriteRenderer)
        {
            spriteRenderer.enabled = true;
            var spriteRendererTransform = spriteRenderer.transform;
            var scale = spriteRendererTransform.localScale;
            var targetScale = Vector3.one * 2f;
            var time = 0f;
            while (time < 1)
            {
                time += 5f * Time.deltaTime / selectTime;
                spriteRendererTransform.localScale = Vector3.Lerp(scale, targetScale, time);
                var color = spriteRenderer.color;
                color.a = Mathf.Lerp(1f, 0f, time);
                spriteRenderer.color = color;
                yield return null;
            }

            ResetSelectAnimation(spriteRenderer);
        }

        private static void ResetSelectAnimation(SpriteRenderer spriteRenderer)
        {
            var initColor = spriteRenderer.color;
            initColor.a = 255f;

            spriteRenderer.transform.localScale = Vector3.one;
            spriteRenderer.color = initColor;
            spriteRenderer.enabled = false;
        }

        private bool Equals(Dot other)
        {
            return base.Equals(other) && ColorIndex == other.ColorIndex && BoardPoint.Equals(other.BoardPoint);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return obj.GetType() == GetType() && Equals((Dot) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = base.GetHashCode();
                hashCode = (hashCode * 397) ^ ColorIndex;
                hashCode = (hashCode * 397) ^ BoardPoint.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"Dot(Color={ColorIndex}, DotPoint={BoardPoint})";
        }

        public void ResetAnimations()
        {
            transform.localScale = Vector3.one;
        }
    }
}