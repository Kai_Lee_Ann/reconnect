﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Service;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core
{
    [RequireComponent(typeof(GameObjectPool))]
    public class Board : MonoBehaviour
    {
        private const int BoardSize = 6;
        private const float DotOffset = 0.5f - BoardSize / 2f;

        [SerializeField] private ColorMapper colorMapper;
        [SerializeField] private DotLineChain dotLineChain;
        [SerializeField] private ScoreService scoreService;

        [Header("Fall Down Settings")] [SerializeField]
        private int aboveScreenRow;

        [SerializeField] private float aboveScreenFallDownStartDelay;
        [SerializeField] private float fallDownStartDelay;
        [SerializeField] private float fallDownDiffTime;
        [SerializeField] private float shuffleDelay = 0.5f;

        private Dot[,] _dots;
        private GameObjectPool _gameObjectPool;

        private void Start()
        {
            _gameObjectPool = GetComponent<GameObjectPool>();
            _dots = new Dot[BoardSize, BoardSize];

            var maxCreateDotDelayTime = CreateDots();
            if (!IsTurnAvailable())
            {
                StartCoroutine(ShuffleBoard(maxCreateDotDelayTime + shuffleDelay));
            }

            dotLineChain.OnChainCompleted += dots => UpdateBoard(new HashSet<Dot>(dots));
            dotLineChain.OnChainCompleted += scoreService.AddScore;
            if (Debug.isDebugBuild)
            {
                dotLineChain.OnChainCompleted += CheckForNullDots;
            }
        }

        private void UpdateBoard(IReadOnlyCollection<Dot> dots)
        {
            ClearDots(dots);
            var dotsFallRows = FindDotsFallRows(dots);
            var minFallDownRow = FindMinFallDownRow(dotsFallRows.Values);
            FallDownDots(dotsFallRows, minFallDownRow);

            var columnRemovedDots = new Dictionary<int, int>();
            foreach (var dot in dots)
            {
                var column = dot.BoardPoint.Column;
                if (columnRemovedDots.TryGetValue(column, out var count))
                {
                    columnRemovedDots[column] = count + 1;
                }
                else
                {
                    columnRemovedDots[column] = 1;
                }
            }

            foreach (var columnRemovedDot in columnRemovedDots)
            {
                var column = columnRemovedDot.Key;
                for (var count = 0; count < columnRemovedDot.Value; count++)
                {
                    _dots[column, BoardSize - 1 - count] = null;
                }
            }
            
            var maxCreateDotDelayTime = CreateDots(minFallDownRow);
            if (!IsTurnAvailable())
            {
                StartCoroutine(ShuffleBoard(maxCreateDotDelayTime + shuffleDelay));
            }
        }

        private static int FindMinFallDownRow(IReadOnlyCollection<int> fallRows)
        {
            return fallRows.Any() ? fallRows.Min() : BoardSize - 1;
        }

        private void CheckForNullDots(List<Dot> dots)
        {
            foreach (var boardPoint in GetNullDots())
            {
                Debug.LogError($"Null cell found!: {boardPoint}");
            }
        }

        private IEnumerable<BoardPoint> GetNullDots()
        {
            for (var column = 0; column < BoardSize; column++)
            {
                for (var row = 0; row < BoardSize; row++)
                {
                    if (ReferenceEquals(_dots[column, row], null))
                    {
                        yield return new BoardPoint(column, row);
                    }
                }
            }
        }

        public IEnumerable<Dot> GetDots(int colorIndex)
        {
            return _dots
                .Cast<Dot>()
                .Where(dot => dot.ColorIndex == colorIndex);
        }

        private void ClearDots(IEnumerable<Dot> dots)
        {
            foreach (var dot in dots)
            {
                this[dot.BoardPoint] = null;
                dot.Destroy(() => _gameObjectPool.Release(dot.gameObject));
            }
        }

        private Dictionary<Dot, int> FindDotsFallRows(IEnumerable<Dot> clearedDots)
        {
            var dotsFallRows = new Dictionary<Dot, int>();
            foreach (var dot in clearedDots)
            {
                var moveColumn = dot.BoardPoint.Column;
                for (var row = dot.BoardPoint.Row + 1; row < BoardSize; row++)
                {
                    var fallDot = _dots[moveColumn, row];
                    if (ReferenceEquals(fallDot, null))
                    {
                        continue;
                    }

                    if (dotsFallRows.TryGetValue(fallDot, out var fallRow))
                    {
                        var newFallRow = fallRow - 1;
                        dotsFallRows[fallDot] = newFallRow;
                    }
                    else
                    {
                        var newFallRow = row - 1;
                        dotsFallRows[fallDot] = newFallRow;
                    }
                }
            }

            return dotsFallRows;
        }

        private void FallDownDots(Dictionary<Dot, int> dotsFallRows, int minFallDownRow)
        {
            foreach (var dotFallRow in dotsFallRows)
            {
                var dot = dotFallRow.Key;
                var targetRow = dotFallRow.Value;
                FallToRow(dot, targetRow, fallDownStartDelay + fallDownDiffTime * (targetRow - minFallDownRow));
            }
        }

        private float CreateDots(int minFallDownRow = 0)
        {
            var maxCreateDotDelayTime = 0f;
            foreach (var emptyDot in GetNullDots())
            {
                var delay = GetCreateDotsDelay(emptyDot, minFallDownRow);
                maxCreateDotDelayTime = Math.Max(maxCreateDotDelayTime, delay);
                CreateDot(emptyDot, delay);
            }

            return maxCreateDotDelayTime;
        }

        private float GetCreateDotsDelay(BoardPoint boardPoint, int minFallDownRow)
        {
            return aboveScreenFallDownStartDelay
                   + fallDownStartDelay
                   + fallDownDiffTime * (boardPoint.Row - minFallDownRow);
        }

        private IEnumerator ShuffleBoard(float delayTime)
        {
            do
            {
                yield return new WaitForSeconds(delayTime);
                ShuffleBoard();
            } while (!IsTurnAvailable());
        }

        public void ShuffleBoard()
        {
            for (var column = BoardSize - 1; column > 0; column--)
            {
                for (var row = BoardSize - 1; row > 0; row--)
                {
                    var newColumn = Random.Range(0, column + 1);
                    var newRow = Random.Range(0, row + 1);

                    var temp = _dots[column, row];
                    _dots[column, row] = _dots[newColumn, newRow];
                    _dots[newColumn, newRow] = temp;

                    temp.BoardPoint = new BoardPoint(newColumn, newRow);
                    temp.Move(ToWorldPoint(new BoardPoint(newColumn, newRow)));
                    _dots[column, row].BoardPoint = new BoardPoint(column, row);
                    _dots[column, row].Move(ToWorldPoint(new BoardPoint(column, row)));
                }
            }
        }

        private bool IsTurnAvailable()
        {
            for (var column = 0; column < BoardSize; column++)
            {
                for (var row = 0; row < BoardSize; row++)
                {
                    var dot = _dots[column, row];
                    if (IsRightDotSameColor(column, row, dot))
                    {
                        return true;
                    }

                    if (IsTopDotSameColor(column, row, dot))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsRightDotSameColor(int column, int row, Dot dot)
        {
            if (column == BoardSize - 1)
            {
                return false;
            }

            return dot.ColorIndex == _dots[column + 1, row].ColorIndex;
        }

        private bool IsTopDotSameColor(int column, int row, Dot dot)
        {
            if (row == BoardSize - 1)
            {
                return false;
            }

            return dot.ColorIndex == _dots[column, row + 1].ColorIndex;
        }


        private void FallToRow(Dot dot, int targetRow, float startDelay)
        {
            var fallBoardPoint = new BoardPoint(dot.BoardPoint.Column, targetRow);
            this[fallBoardPoint] = dot;
            dot.BoardPoint = fallBoardPoint;
            dot.FallDown(ToWorldPoint(fallBoardPoint), startDelay);
        }

        private void CreateDot(BoardPoint boardPoint, float startDelay)
        {
            var dot = _gameObjectPool.GetObject().GetComponent<Dot>();
            dot.BoardPoint = boardPoint;
            dot.ColorIndex = colorMapper.GetRandomColorIndex();
            dot.ResetAnimations();
            dot.OnDotSelected = selectedDot => dotLineChain.OnDotSelected(selectedDot);
            var color = colorMapper.ToColor(dot.ColorIndex);
            dot.GetComponent<SpriteRenderer>().color = color;
            dot.SelectEffectRenderer.color = color;
            this[boardPoint] = dot;

            dot.transform.position = ToWorldPoint(new BoardPoint(boardPoint.Column, aboveScreenRow));
            dot.FallDownFromAbove(ToWorldPoint(boardPoint), startDelay);
        }

        private Dot this[BoardPoint boardPoint]
        {
            get => _dots[boardPoint.Column, boardPoint.Row];
            set => _dots[boardPoint.Column, boardPoint.Row] = value;
        }

        public Vector2 ToWorldPoint(BoardPoint boardPoint)
        {
            var position = transform.position;
            return new Vector2(
                position.x + boardPoint.Column + DotOffset,
                position.y + boardPoint.Row + DotOffset
            );
        }
    }
}