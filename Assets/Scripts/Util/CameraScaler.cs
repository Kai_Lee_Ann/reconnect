﻿using UnityEngine;

namespace Util
{
    [RequireComponent(typeof(Camera))]
    public class CameraScaler : MonoBehaviour
    {
        private const int BoardSize = 6;
        
        private void Awake()
        {
            var mainCamera = GetComponent<Camera>();
            mainCamera.orthographicSize = BoardSize + 1.5f / (mainCamera.aspect * 2);
        }
    }
}