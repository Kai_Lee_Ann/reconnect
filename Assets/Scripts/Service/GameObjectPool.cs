﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Service
{
    public class GameObjectPool : MonoBehaviour
    {
        [SerializeField] private int poolSize;
        [SerializeField] private GameObject prefab;

        private GameObject[] _gameObjects;
        private Queue<int> _freeObjectIndexes;

        private void Awake()
        {
            _gameObjects = new GameObject[poolSize];
            _freeObjectIndexes = new Queue<int>(poolSize);
            
            for (var objectIndex = 0; objectIndex < poolSize; objectIndex++)
            {
                var o = Instantiate(prefab, transform);
                o.SetActive(false);
                _gameObjects[objectIndex] = o;
                _freeObjectIndexes.Enqueue(objectIndex);
            }
        }

        public GameObject GetObject()
        {
            var o = _gameObjects[_freeObjectIndexes.Dequeue()];
            if (o.activeSelf)
            {
                Debug.LogError("Warning! Reusing of already active object! Please, increase the pool size");
            }
            o.SetActive(true);
            return o;
        }

        public void Release(GameObject o)
        {
            o.SetActive(false);
            _freeObjectIndexes.Enqueue(Array.IndexOf(_gameObjects, o));
        }
    }
}