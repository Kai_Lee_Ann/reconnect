﻿using System.Collections.Generic;
using System.Linq;
using Core;
using TMPro;
using UnityEngine;

namespace Service
{
    public class ScoreService : MonoBehaviour
    {
        private const string ScoreKey = "Score";
        
        public TextMeshProUGUI scoreLabel;
        public int scorePerDot;
        public int scorePerDotInClosedChain;

        private int _score;

        public void Awake()
        {
            _score = PlayerPrefs.GetInt(ScoreKey, 0);
            UpdateScoreLabel();
        }

        public void AddScore(List<Dot> dots)
        {
            var distinctDotsCount = dots.Distinct().Count();
            _score += IsChainContainsClosedShape(dots, distinctDotsCount)
                ? distinctDotsCount * scorePerDotInClosedChain
                : dots.Count * scorePerDot;
            UpdateScoreLabel();
        }

        private static bool IsChainContainsClosedShape(List<Dot> dots, int distinctDotsCount)
        {
            return distinctDotsCount != dots.Count;
        }

        private void UpdateScoreLabel()
        {
            scoreLabel.text = $"Score: {_score}";
        }

        private void OnApplicationPause(bool pause)
        {
            PlayerPrefs.SetInt(ScoreKey, _score);
            PlayerPrefs.Save();
        }
        private void OnApplicationQuit()
        {
            PlayerPrefs.SetInt(ScoreKey, _score);
            PlayerPrefs.Save();
        }
    }
}